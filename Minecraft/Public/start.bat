@echo off
:x
	cls
	echo Starting the server in 3 seconds...
	ping 127.0.0.1 > nul
	java -Xms1G -Xmx3G -d64 -jar spigot.jar nogui
goto x
