#!/bin/sh
while true
do
	clear
	
	echo Updating the server...
	sleep 3
	cd ../Spigot/
	wget --show-progress https://hub.spigotmc.org/jenkins/job/BuildTools/lastBuild/artifact/target/BuildTools.jar -O BuildTools.jar
	java -jar BuildTools.jar --rev 1.13.1
	mv spigot-1.13.1.jar ../Public/spigot.jar
	echo Server updated.

	echo Starting the server...
	sleep 3
	cd ../Public/
	java -Xms1G -Xmx3G -d64 -jar spigot.jar nogui
done
