#!/bin/sh
curl -o License.md --progress-bar https://gitlab.com/NatoBoram/Spigot-Server-Starter-Pack/raw/master/LICENSE.md
echo "
Spigot Server Starter Pack : Easy set-up for Spigot Servers. Copyright © 2017 NatoBoram.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
"
curl -o BuildTools.jar --progress-bar https://hub.spigotmc.org/jenkins/job/BuildTools/lastBuild/artifact/target/BuildTools.jar
curl -o BungeeCord.jar --progress-bar http://ci.md-5.net/job/BungeeCord/lastBuild/artifact/bootstrap/target/BungeeCord.jar
java -jar BuildTools.jar
curl -o start.sh --progress-bar https://gitlab.com/NatoBoram/Spigot-Server-Starter-Pack/raw/master/Minecraft/Spigot/start.sh
exit
