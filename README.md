# Spigot Server Starter Pack

The `SSSP` aims at making starting a new Minecraft Server as fast as possible. Here's what's included.

## Plugins

Here's a list of plugins and what they do :

* [AntiCreeper](https://www.spigotmc.org/resources/23174/) prevents Creepers from destroying the world,
* [AntiXRay](https://mods.curse.com/bukkit-plugins/minecraft/anti-x-ray) alerts when people are mining way too many diamonds,
* [GriefPrevention](https://www.spigotmc.org/resources/1884/) prevents grief,
* [PopulationDensity](https://www.spigotmc.org/resources/7995/) spawns new players in areas where there's lots of resources,
* [WesternTea](https://www.spigotmc.org/resources/32061/) prevents world corruption and dead chunks,
* [WesternTea Effects](https://www.spigotmc.org/resources/32114/) adds some cool lightning bolts when players log-in, log-out or die.

## Server Properties

```properties
enable-query=true
prevent-proxy-connections=true
resource-pack-sha1=84faa87fca802aca95eaf4cb39c8a40331e2b3ac
resource-pack=https://addons-origin.cursecdn.com/files/2448/777/Faithful%201.12-rv2.zip
pvp=false
difficulty=2
enable-command-block=true
player-idle-timeout=10
spawn-protection=-1
```

* `enable-query` is enabled because public servers need some visibility and query is the way to go to get quick information about a public server.
* `prevent-proxy-connections` is enabled to facilitage Grief Prevention's ban system across IPs.
* The used `resource-pack` is [Faithful 32x32](https://minecraft.curseforge.com/projects/faithful-vanilla), which is really just an upgrade of the vanilla resource pack.
* `pvp` is set to fasle to further prevent griefing.
* `difficulty` is set to normal to counter-balance the creepers' non-destructive explosions
* `enable-command-block` is useful for any public servers to engage the community with mods-powered artefacts. Also useful for map makers.
* `player-idle-timeout` is enabled to help prevent world corruption and dead chunks.
* `spawn-protection` is disabled because Population Density already takes care of that.